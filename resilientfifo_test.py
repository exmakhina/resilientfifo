#!/usr/bin/env python
# -*- coding: utf-8
# Test

import os
import subprocess
import logging
import time

import pytest

from .resilientfifo import ResilientFIFO
from .debug import fifo_stats


logger = logging.getLogger(__name__)


def debug_fifo(fifo, minval, maxval):
	return

	self = fifo
	fifo_stats(fifo)

	expected = {i for i in range(minval, maxval+1)}
	seen = set()

	logger.debug("Contents:")
	with self.env_ram.begin() as txn:
		logger.debug("0:")
		cur = txn.cursor(db=self.db_ram_0)
		for k, v in cur:
			v = self.loads(v)[1]
			logger.debug("- %s: %s", k.hex(), v)
			seen.add(v)

		logger.debug("1:")
		cur = txn.cursor(db=self.db_ram_1)
		for k, v in cur:
			logger.debug("- %s", k.hex())
			for k, v in self.loads(self.decompress(v)):
				logger.debug("  - %s: %s", k.hex(), v[1])
				seen.add(v[1])

	with self.env_dsk.begin() as txn:
		logger.debug("2:")
		cur = txn.cursor(db=self.db_dsk_0)
		for k, v in cur:
			logger.debug("- %s", k.hex())
			for k, v in self.loads(self.decompress(v)):
				logger.debug("  - %s: %s", k.hex(), v[1])
				seen.add(v[1])

	assert seen == expected


@pytest.fixture
def clean_db():
	for base in ("fecmem", "persistent"):
		for ext in (".lmdb", ".lmdb-lock"):
			if os.path.exists(base + ext):
				os.unlink(base + ext)
	#subprocess.run("dd if=fecmem.lmdb2 of=fecmem.lmdb conv=notrunc", shell=True)
	#subprocess.run("dd if=fecmem.lmdb2 of=persistent.lmdb conv=notrunc", shell=True)

def test_stamp(clean_db):

	fifo = ResilientFIFO()
	with fifo:
		assert fifo._counter == 0
		for i in range(5):
			fifo.put("pouet")

	fifo = ResilientFIFO()
	with fifo:

		assert fifo._counter == 5
		fifo.put("coin")

		assert fifo._counter == 6


def test_put(clean_db):

	fifo = ResilientFIFO()

	with fifo:

		def stamp():
			self = fifo # patch
			return self._counter.to_bytes(2, "big")

		fifo.stamp = stamp


		def passthrough(self, data):
			return data

		fifo.compress = lambda x: passthrough(fifo, x)
		fifo.decompress = lambda x: passthrough(fifo, x)

		j = 0
		for i in range(1000):
			logger.info("")
			logger.info("Put %d", i)
			v = "pouet" * 80, i
			fifo.put(v)

			debug_fifo(fifo, j, i)

			if i % 2 == 1:
				logger.info("")
				logger.info("Get %d", j)
				(k, s), a = fifo.get()
				assert a[1] == j

				debug_fifo(fifo, j, i)

				fifo.delete((k, s))
				j += 1

			debug_fifo(fifo, j, i)

		for i in range(500):
			logger.info("")
			logger.info("Get %d", j)
			(k, s), a = fifo.get()
			assert a[1] == j

			debug_fifo(fifo, j, 999)

			fifo.delete((k, s))
			j += 1

			debug_fifo(fifo, j, 999)
