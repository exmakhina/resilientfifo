from setuptools import setup, find_packages

setup(
	name="xm_resilientfifo",
	packages=find_packages(include=['abcd.efgh']),
	package_dir = {'exmakhina.resilientfifo': '.'},
	py_modules=[
	 "exmakhina.resilientfifo.__init__",
	 "exmakhina.resilientfifo.resilientfifo",
	],
	install_requires=[
	 "lmdb",
	],
)
