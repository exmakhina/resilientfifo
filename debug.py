#!/usr/bin/env python
# SPDX-License-Identifier: GPL-3.0-only OR Proprietary
# SPDX-FileCopyrightText: 2023 Jérôme Carretero <cJ-resilientfifo@zougloub.eu>

def fifo_stats(fifo):
	self = fifo
	if not hasattr(self, "ts"):
		self.ts = time.time()

	now = time.time()
	logger.debug("Sizes:")
	with self.env_ram.begin() as txn:
		s = txn.stat(self.db_ram_0)
		size = (s["branch_pages"] + s["leaf_pages"] + s["overflow_pages"]) * s["psize"]
		size0 = size
		entries = s["entries"]
		logger.debug("0: size=%d entries=%d", size, entries)

		s = txn.stat(self.db_ram_1)
		size = (s["branch_pages"] + s["leaf_pages"] + s["overflow_pages"]) * s["psize"]
		size1 = size
		entries = s["entries"]
		logger.debug("1: size=%d entries=%d", size, entries)

	with self.env_dsk.begin() as txn:
		s = txn.stat(self.db_dsk_0)
		size = (s["branch_pages"] + s["leaf_pages"] + s["overflow_pages"]) * s["psize"]
		size2 = size
		entries = s["entries"]
		logger.debug("2: size=%d entries=%d", size, entries)

	debit = (size1 + size2) / (now - self.ts) * (60 * 60) / 1000
	logger.debug("Compressed debit: %.3f kB/h", debit)

	debit = self.added / (now - self.ts) * (60 * 60) / 1000
	logger.debug("Uncompressed debit: %.3f kB/h", debit)
