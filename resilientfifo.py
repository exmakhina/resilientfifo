#!/usr/bin/env python
# SPDX-License-Identifier: GPL-3.0-only OR Proprietary
# SPDX-FileCopyrightText: 2023 Jérôme Carretero <cJ-resilientfifo@zougloub.eu>

import typing as t
import zlib
import marshal
import os
import logging
import contextlib

import lmdb


logger = logging.getLogger(__name__)


class LazyBlob:
	def __init__(self, data):
		self.v = data

	def __str__(self):
		if len(self.v) > 32:
			return self.v[:32].hex() + "..."
		return self.v.hex()

	def __repr__(self):
		return "{" + str(self) + "}"


class ResilientFIFO:
	"""
	FIFO which is suitable for use on machines with the following
	characteristics:

	- System performs safe shutdowns / cold reboots
	- System can do unexpected reboots (eg. hang and watchdog reboots)
	- System doesn't have ECC memory

	The data pushed on the FIFO is serialized and stored in a tiered fashion:

	- First, it is put in a RAM database in blobs containing a single entry
	- Then this 1st tier may become too big and spill in a 2nd tier,
	  using a database in blobs containing several entries, compressed,
	  also stored a file (normally in RAM).
	- Then this 2nd tier may become too big and spill in a 3rd tier,
	  using a database in blobs containing several entries, compressed,
	  stored in a file (normally in mass storage).

	Notes:

	- This class is not thread-safe.
	- Must use ResilientFIFO objects with context manager, but only once.

	"""
	def __init__(self,
	 mem_path="fecmem.lmdb",
	 persistent_path="persistent.lmdb",
	 ram_map_size: t.Optional[int]=None,
	 dsk_map_size: t.Optional[int]=None,
	 max_size_0: t.Optional[int]=None,
	 max_size_1: t.Optional[int]=None,
	 spill_01: t.Optional[int]=None,
	 spill_12: t.Optional[int]=None,
	 db_ram_0_name: t.Optional[bytes]=b"log-single",
	 db_ram_1_name: t.Optional[bytes]=b"log-packed",
	 db_dsk_0_name: t.Optional[bytes]=b"log-packed",
	 ):
		"""
		Initialize the database.
		There are so many parameters that you're better off overriding
		this code in your class inheriting from ResilientFIFO.

		:param ram_map_size: LMDB mapping size (~ max FS size of the DB) for RAM DB
		:param dsk_map_size: LMDB mapping size (~ max FS size of the DB) for persistent DB
		:param max_size_0: max size of entries in first tier
		:param max_size_1: max size of entries in second tier
		:param spill_01: number of entries to spill in a batch, from first to second tier
		:param spill_12: number of (packed) entries to spill in a batch, from second to third tier

		"""

		self._mem_path = mem_path
		self._persistent_path = persistent_path


		self.stack = contextlib.ExitStack()

		if ram_map_size is None:
			if not os.path.exists(mem_path):
				ram_map_size = 1<<20
			else:
				ram_map_size = os.path.getsize(mem_path) + (1 <<20)

		self._ram_map_size = ram_map_size

		if dsk_map_size is None:
			dsk_map_size = 1<<30

		self._dsk_map_size = dsk_map_size

		if max_size_0 is None:
			max_size_0 = 100<<10

		if max_size_1 is None:
			max_size_1 = 500<<10

		if spill_01 is None:
			spill_01 = 64

		if spill_12 is None:
			spill_12 = 800

		self.max_size_0 = max_size_0
		self.max_size_1 = max_size_1
		self.spill_01 = spill_01
		self.spill_12 = spill_12

		self._db_ram_0_name = db_ram_0_name
		self._db_ram_1_name = db_ram_1_name
		self._db_dsk_0_name = db_dsk_0_name

		self.added = 0

	def _open_envs(self):

		self.env_ram = lmdb.open(self._mem_path,
		 subdir=False,
		 max_dbs=10,
		 map_size=self._ram_map_size,
		)

		self.stack.enter_context(self.env_ram)

		self.env_dsk = lmdb.open(self._persistent_path,
		 subdir=False,
		 max_dbs=10,
		 map_size=self._dsk_map_size,
		)

		self.stack.enter_context(self.env_dsk)

	def _open_dbs(self):
		self.db_ram_0 = self.env_ram.open_db(self._db_ram_0_name,
		 create=True,
		)

		self.db_ram_1 = self.env_ram.open_db(self._db_ram_1_name,
		 create=True,
		)


		self.db_dsk_0 = self.env_dsk.open_db(self._db_dsk_0_name,
		 create=True,
		)

		counter0 = counter1 = counter2 = 0
		with self.env_ram.begin() as txn:
			cur = txn.cursor(db=self.db_ram_0)
			res = cur.last()
			if res:
				counter0 = int.from_bytes(cur.key(), "big")

			cur = txn.cursor(db=self.db_ram_1)
			res = cur.last()
			if res:
				for k,v in self.loads(self.decompress(cur.value())):
					counter1 = int.from_bytes(k, "big")

		with self.env_dsk.begin() as txn:
			cur = txn.cursor(db=self.db_dsk_0)
			res = cur.last()
			if res:
				for k,v in self.loads(self.decompress(cur.value())):
					counter2 = int.from_bytes(k, "big")

		self._counter = max(counter0, counter1, counter2)

		if self._counter > 0:
			logger.info("Loaded counter: %d", self._counter)

	def __enter__(self):
		self.stack.__enter__()
		self._open_envs()
		self._open_dbs()
		return self

	def __exit__(self, *args):
		ret = self.stack.__exit__(*args)
		return ret

	def compress(self, data: bytes) -> bytes:
		"""
		Compress a blob of data. You can override this.
		"""
		return zlib.compress(data)

	def decompress(self, data: bytes) -> bytes:
		"""
		Decompress a blob of data. You can override this.
		"""
		return zlib.decompress(data)

	def loads(self, data: bytes) -> t.Any:
		"""
		Deserializes a blob of data. You can override this.
		"""
		return marshal.loads(data)

	def dumps(self, data: t.Any) -> bytes:
		"""
		Serialize argument into bytes. You can override this.
		"""
		return marshal.dumps(data)

	def _stamp(self):
		return self._counter.to_bytes(8, "big")

	def put(self, data: t.Any):
		"""
		Put one element
		"""
		return self._put(data)

	def _put(self, data, flush=False):
		if not flush:
			self._counter += 1

			k = self._stamp()

			v = self.dumps(data)

			self.added += len(v)

			with self.env_ram.begin(write=True) as txn:
				logger.debug("Put ram %s = %s", LazyBlob(k), LazyBlob(v))
				txn.put(k, v, db=self.db_ram_0)

			with self.env_ram.begin() as txn:
				s = txn.stat(self.db_ram_0)
				size = (s["branch_pages"] + s["leaf_pages"] + s["overflow_pages"]) * s["psize"]
				logger.debug("Uncompressed RAM size: %d", size)
				spill = size > self.max_size_0
		else:
			spill = True

		if not spill:
			return

		else:
			logger.debug("Spill to zram")
			chunks = dict()

			k0 = k1 = k2 = b"\xff" * 8

			chunks_to_spill = self.spill_01 if not flush else -1

			with self.env_ram.begin() as txn:
				cur = txn.cursor(db=self.db_ram_0)
				res = cur.first()
				assert res
				k0 = cur.key()
				n = 0
				for i, (k, z) in enumerate(cur):
					if i == chunks_to_spill:
						break
					v = self.loads(z)
					chunks[k] = v

			logger.debug("Collected %d chunks", len(chunks))

			with self.env_ram.begin(write=True) as txn:

				cur = txn.cursor(db=self.db_ram_1)
				if cur.first():
					k1, v1 = cur.item()
					logger.debug("Key %s vs. %s %d", LazyBlob(k0), LazyBlob(k1), k0 < k1)

				pack_old = []
				pack_new = []
				for k, v in chunks.items():
					if k < k1:
						pack_old.append((k, v))
					else:
						# TODO check assumptions
						pack_new.append((k, v))

				z_old = self.compress(self.dumps(pack_old))
				z_new = self.compress(self.dumps(pack_new))

				if pack_old:
					logger.debug("put old %s", [(LazyBlob(x[0]),x[1][1]) for x in pack_old])
					txn.put(pack_old[0][0], z_old, db=self.db_ram_1)

				for k, v in pack_old:
					txn.delete(k, db=self.db_ram_0)

				if pack_new:
					logger.debug("put new %s", [(LazyBlob(x[0]),x[1][1]) for x in pack_new])
					txn.put(pack_new[0][0], z_new, db=self.db_ram_1)

				for k, v in pack_new:
					txn.delete(k, db=self.db_ram_0)

		if flush:
			spill = True
		else:
			with self.env_ram.begin() as txn:
				s = txn.stat(self.db_ram_1)
				size = (s["branch_pages"] + s["leaf_pages"] + s["overflow_pages"]) * s["psize"]
				logger.debug("Compressed RAM size: %d", size)
				spill = size > self.max_size_1

		if not spill:
			return

		else:
			logger.debug("Spill from zram to disk")

			# if we spill the most recent, they will be popped later
			# but the will still be popped...

			ks = []

			chunks_to_spill = self.spill_12 if not flush else -1

			with (
			  self.env_ram.begin() as txn_ram,
			  self.env_dsk.begin(write=True) as txn_dsk,
			 ):
				cur = txn_ram.cursor(db=self.db_ram_1)
				res = cur.first()
				assert res
				for i, (k, v) in enumerate(cur):
					if i == chunks_to_spill:
						break
					ks.append(k)
					txn_dsk.put(k, v, db=self.db_dsk_0)

			#logger.debug("Keys to delete: %s", ",".join(x.hex() for x in ks))

			with self.env_ram.begin(write=True) as txn:
				for k in ks:
					txn.delete(k, db=self.db_ram_1)


	def get(self) -> t.Tuple[t.Any, t.Any]:
		"""
		Get one element (the earliest), or IndexError if empty
		"""

		k0 = k1 = k2 = kinf = b"\xff" * 8
		v0 = v1 = v2 = b""
		s0, s1, s2 = 0, 1, 2

		with self.env_ram.begin() as txn:
			cur = txn.cursor(db=self.db_ram_0)
			if cur.first():
				k0, v0, s0 = *cur.item(), 0

			cur = txn.cursor(db=self.db_ram_1)
			if cur.first():
				k1, v1, s1, = *cur.item(), 1

		with self.env_dsk.begin() as txn:
			cur = txn.cursor(db=self.db_dsk_0)
			if cur.first():
				k2, v2, s2 = *cur.item(), 2

		#logger.debug("0: %s %s %s", k0, v0, s0)
		#logger.debug("1: %s %s %s", k1, v1, s1)
		#logger.debug("2: %s %s %s", k2, v2, s2)

		k, v, s = min((k0, v0, s0), (k1, v1, s1), (k2, v2, s2))

		#logger.debug("Item: %s %s %s", k, v, s)

		if k is kinf:
			raise IndexError("Empty FIFO")

		if s in (1,2):
			# Get the first item from the bunch
			v = self.loads(self.decompress(v))[0][1]
		else:
			v = self.loads(v)

		return (k, s), v

	def delete(self, key):
		"""
		Delete the designated element

		:param k: element key
		:param s: element storage area
		 (0 is uncompressed RAM, 1 is compressed RAM, 2 is disk)
		"""
		k, s = key

		if s == 0:
			"""
			Nothing special happens when deleting from the uncompressed RAM
			fifo.
			"""
			with self.env_ram.begin(write=True) as txn:
				cur = txn.cursor(db=self.db_ram_0)
				res = cur.first()
				assert res
				assert cur.key() == k, \
				 f"When trying to delete {k.hex()} from uncompressed RAM," \
				 f"found that the first element is {cur.key().hex()}"
				txn.delete(k, db=self.db_ram_0)
		elif s == 1:
			"""
			When popping from compressed RAM, as we only delete the first
			element of a pack, we need to reinject the rest of the pack's
			elements, and we do so in uncompressed RAM.
			"""
			with self.env_ram.begin(write=True) as txn:
				cur = txn.cursor(db=self.db_ram_1)
				res = cur.first()
				assert res
				assert cur.key() == k
				v = cur.value()
				v = self.loads(self.decompress(v))
				assert v == sorted(v)
				assert v[0][0] == k
				for k_, v_ in v[1:]:
					v_ = self.dumps(v_)
					txn.put(k_, v_, db=self.db_ram_0)
				cur.delete()
		elif s == 2:
			"""
			When popping from disk, as we only delete the first
			element of a pack, we need to reinject the rest of the pack's
			elements, and we do so in uncompressed RAM.
			"""
			with (
			  self.env_dsk.begin(write=True) as txn_dsk,
			  self.env_ram.begin(write=True) as txn_ram,
			 ):
				cur = txn_dsk.cursor(db=self.db_dsk_0)
				res = cur.first()
				assert res
				assert cur.key() == k
				v = cur.value()
				v = self.loads(self.decompress(v))
				assert v[0][0] == k
				assert v == sorted(v)
				for k_, v_ in v[1:]:
					v_ = self.dumps(v_)
					txn_ram.put(k_, v_, db=self.db_ram_0)
				cur.delete()

				# TODO maybe we want to write more to RAM,
				# to reduce the amount of transactions?

	# TODO iterate, if needed?

	def empty(self) -> bool:
		"""
		Tell whether the FIFO is empty
		"""
		with self.env_ram.begin() as txn:
			s = txn.stat(self.db_ram_0)
			entries = s["entries"]
			if entries > 0:
				return False

			s = txn.stat(self.db_ram_1)
			entries = s["entries"]
			if entries > 0:
				return False

		with self.env_dsk.begin() as txn:
			s = txn.stat(self.db_dsk_0)
			entries = s["entries"]
			if entries > 0:
				return False

		return True

	def flush(self):
		"""
		Move everything to persistent storage
		"""
		self._put(None, flush=True)
